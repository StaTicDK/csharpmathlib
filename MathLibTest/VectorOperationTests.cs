using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MathLib;
namespace MathLibTest;

[TestClass]
public class VectorOperationTests
{
    [TestMethod]
    public void VectorMultipliedByScalar()
    {
        Vector v = new (new double[]{2, 3});
        Vector w = new (new [] {Math.PI, Math.PI});
        Vector h = new (new double[] {1, 2, 3, 4, 5, 6, 7});
        Assert.AreEqual(new Vector(new double[] {4, 6}), v * 2);
        Assert.AreEqual(new Vector(new [] {5, 7.5}), v * 2.5);
        Assert.AreEqual(new Vector(new double[] {1, 1}) * Math.PI, w);
        Assert.AreEqual(new Vector(new double[] {3, 6, 9, 12, 15, 18, 21}), h * 3);
    }

    [TestMethod]
    public void VectorAddAndSubOperations()
    {
        
    }
}