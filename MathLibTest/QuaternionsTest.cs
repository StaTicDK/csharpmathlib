﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MathLib;
namespace MathLibTest;

[TestClass]
public class QuaternionsTest
{
    [TestMethod]
    public void PureQuaternion()
    {
        Quaternion q = new Quaternion(0, new Vector(new double[] {1, 2, 3}));
        Assert.AreEqual(true, q.IsPure());
    }

    [TestMethod]
    public void NormalizeQuaternion()
    {
        Quaternion q = new Quaternion(1, new Vector(new double[] {4, 4, -4}));
        q.Normalize();
        Assert.AreEqual(new Quaternion(1.0/7.0, new Vector(new [] {4.0/7.0, 4.0/7.0, 4.0/7.0})), q);

        q = new Quaternion(Math.Sqrt(2) / 2, false, false,true);
        Quaternion p = new(0, new Vector(new double[] {2, 0, 0}));
        Quaternion t = new(0, new Vector(new [] {Math.Sqrt(2), Math.Sqrt(2), 0}));
        // Quaternion multiplication test
    }
}