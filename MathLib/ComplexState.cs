﻿namespace MathLib;

public enum ComplexState
{
    Y,
    Nx,
    Ny,
    X
}