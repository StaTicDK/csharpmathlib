﻿namespace MathLib;

public class Vector
{
    private readonly double[] _components;
    
    // Memebers of Vector
    
    public double this[int i]
    {
        get => _components[i];
        set => _components[i] = value;
    }

    public int Size => _components.Length;
    
    /// <summary>
    /// Takes in an array of doubles and stores each value as a dimension in the vector,
    /// thus an array of size n, will be converted to a n-dimensional vector.
    /// </summary>
    /// <param name="components"></param>
    public Vector(double[] components)
    {
        this._components = components;
    }

    /// <summary>
    /// Returns the Dot product of two vectors if vectors have same number of dimensions
    /// </summary>
    /// <param name="v"></param>
    /// <param name="w"></param>
    /// <returns>Dot product</returns>
    /// <exception cref="VectorNotOfSameSizeException"></exception>
    public static double Dot(Vector v, Vector w)
    {
        if (v.Size != w.Size)
            throw new VectorNotOfSameSizeException("The vector \"v\" has size of " + 
                                                   v.Size + 
                                                   " but was compared to vector \"w\" of size " + w.Size);

        double result = 0;

        for (int i = 0; i < v.Size; i++)
            result += v[i] * w[i];

        return result;
    }
    /// <summary>
    /// Returns the Cross product of two vectors if they are of same size,
    /// and have exactly 3 dimensions
    /// </summary>
    /// <param name="v"></param>
    /// <param name="w"></param>
    /// <returns>The cross product vector</returns>
    /// <exception cref="VectorNotOfSameSizeException"></exception>
    /// <exception cref="WrongDimensionException"></exception>
    public static Vector Cross(Vector v, Vector w)
    {
        if (v.Size != w.Size) 
            throw new VectorNotOfSameSizeException("The vector \"v\" has size of " + 
                                                   v.Size + 
                                                   " but was compared to vector \"w\" of size " + w.Size);
        if (v.Size != 3)
            if (v.Size > 3)
                throw new WrongDimensionException(
                    "Trying to use Cross product on vectors that have more than 3 dimensions");
            else
                throw new WrongDimensionException(
                    "Trying to use Cross product on vectors that have less than 3 dimensions");

        double x = (v[1] * w[2] - v[2] * w[1]);
        double y = -(v[0] * w[2] - v[2] * w[0]);
        double z = (v[0] * w[1] - v[1] * w[0]);

        return new Vector(new [] {x, y, z});
    }
    
    /// <summary>
    /// Returns if the vector is a zero vector or not
    /// </summary>
    /// <param name="v"></param>
    /// <returns>true if all dimensions are 0, false otherwise</returns>
    public static bool IsZero(Vector v)
    {
        for (int i = 0; i < v.Size; i++)
            if (v[i] != 0)
                return false;
        return true;
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private double Length()
    {
        double length = 0;

        for (int i = 0; i < Size; i++)
            length += Math.Pow(this[i], 2);

        return Math.Sqrt(length);
    }

    public double SquaredLength()
    {
        double length = 0;

        for (int i = 0; i < Size; i++)
            length += Math.Pow(this[i], 2);

        return length;
    }

    public void Normalize()
    {
        double length = Length();
        for (int i = 0; i < Size; i++)
        {
            this[i] *= 1 / length;
        }
    }
    
    public static Vector operator *(double s, Vector v)
    {
        double[] components = new double[v.Size];
        Array.Copy(v._components, components, v.Size);

        for (int i = 0; i < v.Size; i++)
            components[i] *= s;

        return new Vector(components);
    }

    public static Vector operator *(Vector v, double s)
    {
        double[] components = new double[v.Size];
        Array.Copy(v._components, components, v.Size);

        for (int i = 0; i < v.Size; i++)
            components[i] *= s;

        return new Vector(components);
    }

    public static Vector operator +(Vector v, Vector w)
    {
        if (v.Size != w.Size)
            throw new VectorNotOfSameSizeException("The vector \"v\" has size of " + 
                                                   v.Size + 
                                                   " but was compared to vector \"w\" of size " + w.Size);

        double[] components = new double[v.Size];

        for (int i = 0; i < v.Size; i++)
            components[i] = v[i] + w[i];

        return new Vector(components);
    }

    public static Vector operator -(Vector v, Vector w)
    {
        if (v.Size != w.Size)
            throw new VectorNotOfSameSizeException("The vector \"v\" has size of " + 
                                                   v.Size + 
                                                   " but was compared to vector \"w\" of size " + w.Size);

        double[] components = new double[v.Size];
        for (int i = 0; i < v.Size; i++)
            components[i] = v[i] - w[i];

        return new Vector(components);
    }
    
    public static bool operator ==(Vector? v, Vector? w)
    {
        if (v is null || w is null)
            throw new NullVectorException("Cannot compare vectors with null...");

        if (v.Size != w.Size)
            throw new VectorNotOfSameSizeException("Cannot compare two vectors of unequal size");

        for (int i = 0; i < v.Size; i++)
        {
            if (Math.Abs(v[i] - w[i]) > 0.01)
            {
                return false;
            }
        }

        return true;
    }

    public static bool operator !=(Vector? v, Vector? w)
    {
        return !(v == w);
    }
    
    public override bool Equals(object? obj)
    {
        if (obj is not Vector temp)
            throw new NullVectorException("Cannot compare vectors with null...");

        if (temp.Size != Size)
            throw new VectorNotOfSameSizeException("Cannot compare two vectors of unequal size");
        for (int i = 0; i < Size; i++)
        {
            if (Math.Abs(this[i] - temp[i]) > 0.01)
            {
                return false;
            }
        }

        return true;
    }

    public override int GetHashCode()
    {
        return _components.GetHashCode();
    }
}