﻿namespace MathLib;

public class ComplexNumber
{
    private double _a;
    private double _b;
    private readonly bool _isRotor;

    public ComplexNumber(double a, double b)
    {
        this._a = a;
        this._b = b;
        this._isRotor = false;
    }

    public ComplexNumber(double theta)
    {
        this._a = Math.Cos(theta);
        this._b = Math.Sin(theta);
        this._isRotor = true;
    }

    public static ComplexNumber Add(ComplexNumber q, ComplexNumber p)
    {
        return new ComplexNumber(q._a + p._a, q._b + p._b);
    }

    public static ComplexNumber Sub(ComplexNumber q, ComplexNumber p)
    {
        return new ComplexNumber(q._a - p._a, q._b - p._b);
    }

    public void Multiply(long s)
    {
        this._a *= s;
        this._b *= s;
    }

    public static ComplexNumber Product(ComplexNumber q, ComplexNumber p)
    {
        if (!q._isRotor || !p._isRotor)
        {
            return new ComplexNumber(q._a * p._a - q._b * p._b, q._a * p._b + q._b * p._a);
        }

        return new ComplexNumber(q._a * p._b - p._a * q._b, q._a * p._b + q._b * p._a);
    }

    public void Square()
    {
        double tempA = this._a;
        this._a = (Math.Pow(_a, 2) - Math.Pow(_b, 2));
        this._b = 2 * tempA * _b;
    }

    public void Conjugate()
    {
        this._b *= -1;
    }

    public double Magnitude()
    {
        return Math.Sqrt(Math.Pow(this._a, 2) + Math.Pow(this._b, 2));
    }

    public static ComplexNumber Quotient(ComplexNumber q, ComplexNumber p)
    {
        double a = ((q._a * p._a + q._b + p._b) / (Math.Pow(p._a, 2) + Math.Pow(p._b, 2)));
        double b = ((q._b * p._a - q._a * p._b) / (Math.Pow(p._a, 2) + Math.Pow(p._b, 2)));

        return new ComplexNumber(a, b);
    }
}