﻿namespace MathLib;

public class VectorNotOfSameSizeException : Exception
{
    public VectorNotOfSameSizeException()
    {
        
    }

    public VectorNotOfSameSizeException(string message)
    : base(message)
    {
        
    }

    public VectorNotOfSameSizeException(string message, Exception inner)
    : base(message, inner)
    {
        
    }
}