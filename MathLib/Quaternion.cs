﻿namespace MathLib;

public class Quaternion
{
    private Vector _v;
    private double _s;
    private double _angle;

    private double Norm => Math.Sqrt(Math.Pow(_s, 2) + _v.SquaredLength());

    public Quaternion(double s, Vector v)
    {
        if (v.Size != 3)
            throw new Exception();
        
        this._s = s;
        this._v = v;
        this._angle = 0;
    }

    public Quaternion(double theta, bool i = false, bool j = false, bool k = false)
    {
        this._s = Math.Cos(theta);
        this._v = new Vector(new double[] {1, 1, 1});
        this._angle = theta;
        
        if (!i)
            this._v[0] = 0;
        else
            this._v[0] = Math.Sin(theta);
        if (!j)
            this._v[1] = 0;
        else
            this._v[1] = Math.Sin(theta);
        if (!k)
            this._v[2] = 0;
        else
            this._v[2] = Math.Sin(theta);
    }

    public static Quaternion operator +(Quaternion q, Quaternion p)
    {
        Vector v = q._v + p._v;
        double s = q._s + p._s;
        return new Quaternion(s, v);
    }

    public static Quaternion operator -(Quaternion q, Quaternion p)
    {
        Vector v = q._v - p._v;
        double s = q._s - p._s;
        return new Quaternion(s, v);
    }

    private static double Dot(Quaternion q, Quaternion p)
    {
        return q._s * p._s + Vector.Dot(q._v, p._v);
    }

    public static Quaternion Product(ref Quaternion q, ref Quaternion p)
    {
        double s = q._s * p._s - Vector.Dot(q._v, p._v);
        Vector v = q._s * p._v + p._s * q._v + Vector.Cross(q._v, p._v);

        return new Quaternion(s, v);
    }

    public bool IsReal()
    {
        return Vector.IsZero(this._v);
    }

    public static Quaternion operator *(double s, Quaternion q)
    {
        return new Quaternion(s * q._s, q._v * s);
    }

    public static Quaternion operator *(Quaternion q, double s)
    {
        return new Quaternion(s * q._s, q._v * s);
    }

    public bool IsPure()
    {
        return _s == 0.0;
    }

    private void Conjugate()
    {
        this._v *= -1;
    }

    public void Normalize()
    {
        double scale = this.Norm;
        this._s *= 1 / scale;
        this._v *= 1 / scale;
    }

    public static Quaternion Inverse(Quaternion q)
    {
        q.Conjugate();

        q._s *= 1 / Math.Pow(q.Norm, 2);
        q._v *= 1 / Math.Pow(q.Norm, 2);
        return q;
    }

    public static double AngularAngleBetween(Quaternion q, Quaternion p)
    {
        return Math.Cos(Quaternion.Dot(q, p) / (q.Norm * p.Norm));
    }

    public static Quaternion Rotate(ref Quaternion q, ref Quaternion p)
    {
        Quaternion t = Quaternion.Product(ref q, ref p);

        if (t.IsPure())
            return t;
        else
        {
            if (Math.Abs(q._angle) >= 0.01)
            {
                Quaternion q_new = new Quaternion(q._angle / 2, q._v);
                Quaternion q_inv = Quaternion.Inverse(q_new);
                Quaternion t_new = Quaternion.Product(ref q_new, ref p);
                Quaternion new_t = Quaternion.Product(ref t_new, ref q_inv);
                return new_t;
            }
            else
            {
                Quaternion p_new = new Quaternion(p._angle / 2, p._v);
                Quaternion p_inv = Quaternion.Inverse(p_new);
                Quaternion t_new = Quaternion.Product(ref p_new, ref q);
                Quaternion new_t = Quaternion.Product(ref t_new, ref p_inv);
                return new_t;
            }
        }
    }
    
    public static bool operator ==(Quaternion? q, Quaternion? p)
    {
        if (q is null || p is null)
        {
            throw new Exception();
        }

        if (Math.Abs(q._s - p._s) > 0.001f)
            return false;
        
        
        for (int i = 0; i < q._v.Size; i++)
        {
            if (Math.Abs(q._v[i] - p._v[i]) > 0.001f)
            {
                return false;
            }
        }
        return true;
    }

    public static bool operator !=(Quaternion? q, Quaternion? p)
    {
        return !(q == p);
    }
}