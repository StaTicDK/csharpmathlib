﻿namespace MathLib;

public class WrongDimensionException : Exception
{
    public WrongDimensionException() {}
    
    public WrongDimensionException(string message) : base(message) {}
    
    public WrongDimensionException(string message, Exception inner) : base(message, inner) {}
}