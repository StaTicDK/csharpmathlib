﻿namespace MathLib;

public class NullVectorException : Exception
{
    public NullVectorException() {}
    public NullVectorException(string message) : base(message) {}
    public NullVectorException(string message, Exception inner) : base(message, inner) {}
}